package com.devcamp.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
//@RequestMapping("/combomenu")
public class CComboMenu {
    
	@GetMapping("/devcamp-pizza365")
    public static ArrayList<CMenu> getMenuList() {
        ArrayList<CMenu> menuList = new ArrayList<>();
        CMenu menu1 = new CMenu('S', (byte) 20, (byte) 2, (short) 200, (byte) 2, 150000);
        CMenu menu2 = new CMenu('M', (byte) 25, (byte) 4, (short) 300, (byte) 3, 200000);
        CMenu menu3 = new CMenu('L', (byte) 30, (byte) 8, (short) 500, (byte) 4, 250000);
        menuList.add(menu1);
        menuList.add(menu2);
        menuList.add(menu3);
        return menuList;
    }
}

